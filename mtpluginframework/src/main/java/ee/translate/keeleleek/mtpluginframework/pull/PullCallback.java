package ee.translate.keeleleek.mtpluginframework.pull;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtpluginframework.chopup.TemplateElement;
import ee.translate.keeleleek.mtpluginframework.chopup.TemplateParameterElement;
import ee.translate.keeleleek.mtpluginframework.chopup.WikilinkElement;
import ee.translate.keeleleek.mtpluginframework.mapping.PullMapping;
import ee.translate.keeleleek.mtpluginframework.mapping.PullTemplateMapping;
import ee.translate.keeleleek.mtpluginframework.mapping.PullTemplateParameterMapping;
import ee.translate.keeleleek.mtpluginframework.mapping.PullWikilinkMapping;

/**
 * Mediates pulling.
 */
public abstract class PullCallback {

	public enum Status { UNRESOLVED, CONFIRMED, DECLINED };
	
	private int current = -1;
	private int total = 0;
	private Status status = Status.UNRESOLVED;
	private boolean finished = false;
	private boolean saveTemplateMappings = false;
	
	private ArrayList<PullWikilinkMapping> wikilinkMappings = new ArrayList<>();
	private ArrayList<PullTemplateMapping> templateMappings = new ArrayList<>();
	private ArrayList<PullTemplateParameterMapping> templateParameterMappings = new ArrayList<>();
	
	
	// INIT
	public void init(int total)
	 {
		this.current = -1;
		this.total = total;
		action("");
		advance();
	 }
	
	
	// PROGRESS CALLBACK
	/**
	 * Changes current action.
	 * 
	 * @param action current action
	 */
	public void action(String action)
	 {
		onAction(action);
	 }
	
	/**
	 * Advances progress.
	 */
	public void advance()
	 {
		current++;
		
		if (current == 0) onStart(total);
		onAdvance(current);
		if (current == total) onEnd();
	 }

	/**
	 * Advances progress.
	 * 
	 * @param count count to advance by
	 */
	public void advance(int count)
	 {
		current+= count;

		if (current == 0) onStart(total);
		onAdvance(current);
		if (current == total) onEnd();
	 }

	/**
	 * Finishes the progress.
	 * 
	 * @param success true if successful
	 */
	public void finish(boolean success)
	 {
		finished = true;
		onFinish(success);
	 }
	
	/**
	 * Gets the current progress.
	 * 
	 * @return current progress, -1 if none
	 */
	public int getCurrent() {
		return current;
	}
	
	/**
	 * Gets total progress.
	 * 
	 * @return total progress
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * Checks if the progress is finished
	 * 
	 * @return true if finished
	 */
	public boolean isFinished() {
		return finished;
	}
	
	
	// STATUS CALLFRONT
	/**
	 * Declines the pull.
	 */
	public void decline()
	 {
		this.status = Status.DECLINED;
	 }

	/**
	 * Confirms the pull/
	 */
	public void confirm()
	 {
		this.status = Status.CONFIRMED;
	 }

	/**
	 * Gets pull status.
	 * 
	 * @return pull status
	 */
	public Status getStatus() {
		return status;
	}
	
	/**
	 * Check is pull is declined.
	 * 
	 * @return true if declined
	 */
	public boolean isDeclined() {
		return status == Status.DECLINED;
	}
	
	/**
	 * Check is pull is confirmed.
	 * 
	 * @return true if confirmed
	 */
	public boolean isConfirmed() {
		return status == Status.CONFIRMED;
	}
	
	/**
	 * Check is pull is resolved.
	 * 
	 * @return true if resolved
	 */
	public boolean isResolved() {
		return status != Status.UNRESOLVED;
	}
	
	
	// MAPPING CALLFRONT
	/**
	 * Set template parameter mappings for saving.
	 */
	public void saveTemplateMappings() {
		this.saveTemplateMappings = true;
	}

	/**
	 * Check is template parameters should be saved.
	 * 
	 * @return true if saved
	 */
	public boolean isSaveTemplateMappings() {
		return saveTemplateMappings;
	}
	
	
	/**
	 * Gets wikilink mappings.
	 * 
	 * @return wikilinks mappings
	 */
	public ArrayList<PullWikilinkMapping> getWikilinkMappings() {
		return wikilinkMappings;
	}
	
	/**
	 * Gets template mappings.
	 * 
	 * @return template mappings
	 */
	public List<PullTemplateMapping> getTemplateMappings() {
		return templateMappings;
	}

	/**
	 * Gets template parameter mappings.
	 * 
	 * @return template parameter mappings
	 */
	public ArrayList<PullTemplateParameterMapping> getTemplateParameterMappings() {
		return templateParameterMappings;
	}

	
	// MAPPING
	/**
	 * Maps a wikilink.
	 * 
	 * @param namespace wikilink namespace
	 * @param link wikilink link
	 * @param element wikilink element
	 */
	public void map(String namespace, String link, WikilinkElement element)
	 {
		PullWikilinkMapping mapping = retrieveWikilinkMapping(namespace, link);
		mapping.addWikilink(element);
		
		if (mapping.getElementCount() == 1) onMap(mapping);
	 }

	/**
	 * Maps a template.
	 * 
	 * @param name template name
	 * @param element template element
	 */
	public void map(String name, TemplateElement element)
	 {
		PullTemplateMapping mapping = retrieveTemplateMapping(name);
		mapping.addTemplate(element);
		
		if (mapping.getElementCount() == 1) onMap(mapping);
	 }

	/**
	 * Maps a template parameter.
	 * 
	 * @param templateName template name
	 * @param parameterName parameter name
	 * @param element template parameter element
	 */
	public void map(String templateName, String parameterName, TemplateParameterElement element)
	 {
		PullTemplateParameterMapping mapping = retrieveTemplateParameterMapping(templateName, parameterName);
		mapping.addTemplateParameter(element);
		
		if (mapping.getElementCount() == 1) onMap(mapping);
	 }
	
	
	// EVENTS (PROGRESS)
	/**
	 * Called when the progress starts.
	 * 
	 * @param total progress total
	 */
	protected abstract void onStart(int total);

	/**
	 * Called when the action changes.
	 * 
	 * @param action action
	 */
	protected abstract void onAction(String action);

	/**
	 * Called when the progress advances.
	 * 
	 * @param current current progress
	 */
	protected abstract void onAdvance(int current);

	/**
	 * Called when progress ends, but not yet finishes.
	 * Additional {@link #advance(int)} may restart the progress.
	 */
	protected abstract void onEnd();

	/**
	 * Called when the progress finishes.
	 * 
	 * @param success true if successful
	 */
	protected abstract void onFinish(boolean success);
	
	
	// EVENTS (MAPPING)
	/**
	 * Called when mapping is made.
	 * 
	 * @param mapping mapping
	 */
	protected abstract void onMap(PullMapping mapping);

	
	// HELPERS
	private PullWikilinkMapping retrieveWikilinkMapping(String namespace, String link)
	 {
		for (PullWikilinkMapping mapping : wikilinkMappings) {
			if (mapping.getNamespace().equals(namespace) && mapping.getLink().equals(link)) return mapping;
		}
		
		PullWikilinkMapping mapping = new PullWikilinkMapping(namespace, link);
		wikilinkMappings.add(mapping);
		
		return mapping;
	 }

	private PullTemplateMapping retrieveTemplateMapping(String name)
	 {
		for (PullTemplateMapping mapping : templateMappings) {
			if (mapping.getName().equals(name)) return mapping;
		}
		
		PullTemplateMapping mapping = new PullTemplateMapping(name);
		templateMappings.add(mapping);
		
		return mapping;
	 }
	
	private PullTemplateParameterMapping retrieveTemplateParameterMapping(String templateName, String parameterName)
	 {
		for (PullTemplateParameterMapping mapping : templateParameterMappings) {
			if (mapping.getTemplateName().equals(templateName) && mapping.getParameterName().equals(parameterName)) return mapping;
		}
		
		PullTemplateParameterMapping mapping = new PullTemplateParameterMapping(templateName, parameterName);
		templateParameterMappings.add(mapping);
		
		return mapping;
	 }

	
}

package ee.translate.keeleleek.mtpluginframework.spellcheck;

import java.util.Collection;
import java.util.List;

import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugin;

/**
 * Interface for spell check plugins.
 */
public interface SpellerPlugin extends MinorityTranslatePlugin {
	
	/**
	 * Spell checks.
	 * 
	 * @param langCode language code
	 * @param text text to spell check
	 * @param misspells misspell collection to collect to
	 * @return true if successful
	 * @throws Exception when spell check fails
	 */
	public boolean spellCheck(String langCode, String text, Collection<Misspell> misspells) throws Exception;
	
	/**
	 * Finds suggestions for the given text.
	 * 
	 * @param langCode language code
	 * @param misspell misspell
	 * @return suggestions
	 * @throws when suggestion lookup fails
	 */
	public List<String> findSuggestions(String langCode, Misspell misspell) throws Exception;
	
	
}

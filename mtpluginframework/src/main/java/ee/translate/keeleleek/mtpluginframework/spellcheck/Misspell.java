package ee.translate.keeleleek.mtpluginframework.spellcheck;

import java.util.List;

/**
 * Misspell to show.
 */
public class Misspell {

	private int startRow;
	private int startCol;
	private int endRow;
	private int endCol;
	private String word;
	private String message;
	private List<String> replacements;
	
	
	/**
	 * Constructs a misspell.
	 * 
	 * @param startRow start row
	 * @param startCol start column
	 * @param endRow end row
	 * @param endCol end column
	 * @param misspelled word
	 * @param message message
	 * @param replacements suggested replacements
	 */
	public Misspell(int startRow, int startCol, int endRow, int endCol, String word, String message, List<String> replacements)
	 {
		this.startRow = startRow;
		this.startCol = startCol;
		this.endRow = endRow;
		this.endCol = endCol;
		this.word = word;
		this.message = message;
		this.replacements = replacements;
	 }

	/**
	 * Constructs a misspell.
	 * 
	 * @param startRow start row
	 * @param startCol start column
	 * @param endRow end row
	 * @param endCol end column
	 * @param misspelled word
	 * @param message message
	 */
	public Misspell(int startRow, int startCol, int endRow, int endCol, String word, String message)
	 {
		this.startRow = startRow;
		this.startCol = startCol;
		this.endRow = endRow;
		this.endCol = endCol;
		this.word = word;
		this.message = message;
	 }
	

	/**
	 * Gets start row.
	 * 
	 * @return start row
	 */
	public int getStartRow() {
		return startRow;
	}
	
	/**
	 * Gets start column.
	 * 
	 * @return start column
	 */
	public int getStartColumn() {
		return startCol;
	}
	
	/**
	 * Gets end row.
	 * 
	 * @return end row
	 */
	public int getEndRow() {
		return endRow;
	}
	
	/**
	 * Gets end column.
	 * 
	 * @return end column
	 */
	public int getEndColumn() {
		return endCol;
	}

	/**
	 * Gets the misspelled word.
	 * 
	 * @return
	 */
	public String getWord() {
		return word;
	}
	
	/**
	 * Checks if a message is available.
	 * 
	 * @return true if available
	 */
	public boolean hasMessage() {
		return message != null && !message.isEmpty();
	}
	
	/**
	 * Gets the message.
	 * 
	 * @return message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Gets replacement suggestions.
	 * 
	 * @return suggestions
	 */
	public List<String> getReplacements() {
		return replacements;
	}
	
	
}

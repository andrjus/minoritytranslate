// based on https://github.com/swenson/ace_spell_check_js

var markers = [];

function clearMisspells() {
	var session = editor.session;
	for (var i in markers) {
    	session.removeMarker(markers[i]);
    }
    markers = [];
}

function spellcheck(line) {
//	var words = line.split(' ');
//	var i = 0;
//	var bads = [];
//	for (word in words) {
//	    var x = words[word] + "";
//	    var checkWord = x.replace(/[^a-zA-Z']/g, '');
//	    if (!mediator.spellcheck(checkWord)) {
//	        bads[bads.length] = [i, i + words[word].length];
//	    }
//	    i += words[word].length + 1;
//    }
//    return bads;
}

function markMisspell(row1, col1, row2, col2) {
    var range = new Range(row1, col1, row2, col2);
    markers[markers.length] = editor.session.addMarker(range, "misspelled", "typo", true);
};

function mark2() {
    var session = editor.session;

    // clear the markers
    for (var i in markers) {
      session.removeMarker(markers[i]);
    }
    markers = [];

	// spellcheck and mark
	var lines = session.getDocument().getAllLines();
	for (var i in lines) {
	    var misspellings = spellcheck(lines[i]);
	    
	    for (var j in misspellings) {
	        var range = new Range(i, misspellings[j][0], i, misspellings[j][1]);
	        markers[markers.length] = session.addMarker(range, "misspelled", "typo", true);
	    }
	}
};
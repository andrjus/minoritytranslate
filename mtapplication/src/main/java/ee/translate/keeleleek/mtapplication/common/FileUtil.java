package ee.translate.keeleleek.mtapplication.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.view.dialogs.ManualDialogMediator;

public class FileUtil {
	
	public final static Charset ENCODING = StandardCharsets.UTF_8;
	
	
	private static Logger LOGGER = LoggerFactory.getLogger(ManualDialogMediator.class);


	public static String read(Path path) throws IOException {
		LOGGER.info("Reading " + path);
		StringBuffer data = new StringBuffer();
		try (Scanner scanner = new Scanner(path, ENCODING.name())) {
			while (scanner.hasNextLine()) {
				if (data.length() > 0) data.append('\n');
				data.append(scanner.nextLine());
			}
		}
		return data.toString();
	}
	
	public static String readFromJar(String resource) throws IOException {
		StringBuffer json = new StringBuffer();
		InputStream in = FileUtil.class.getResourceAsStream(resource);
		BufferedReader input = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
		String line;
		while ((line = input.readLine()) != null) {
			if (json.length() > 0) json.append('\n');
			json.append(line);
		}
		return json.toString();
	}
	
	public static void write(Path path, String data) throws IOException {
		LOGGER.info("Writing " + path);
		File file = path.toFile();
		File dir = file.getParentFile();
		if (dir != null) dir.mkdirs();
		file.createNewFile();
		try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
			writer.write(data);
		}
	}
	
}

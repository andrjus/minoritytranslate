package ee.translate.keeleleek.mtapplication.model.autocomplete;

import ee.translate.keeleleek.mtapplication.common.requests.SpellingSuggestRequest;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class AutocompleteRequest {

	private Reference ref;
	private String preceedingText;
	private String selectedText;
	
	private SpellingSuggestRequest suggestRequest = null;
	
	
	public AutocompleteRequest(Reference ref, String preceedingText, String selectedText, SpellingSuggestRequest suggestRequest) {
		super();
		this.ref = ref;
		this.preceedingText = preceedingText;
		this.selectedText = selectedText;
		this.suggestRequest = suggestRequest;
	}
		
	public AutocompleteRequest(Reference ref, String preceedingText, String selectedText) {
		this(ref, preceedingText, selectedText, null);
	}
	

	public Reference getRef() {
		return ref;
	}

	public String getPreceeding() {
		return preceedingText;
	}

	public String getSelected() {
		return selectedText;
	}
	
	public SpellingSuggestRequest getSuggestRequest() {
		return suggestRequest;
	}

	
}

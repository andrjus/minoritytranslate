package ee.translate.keeleleek.mtapplication.model.plugins;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.SpellCheckRequest;
import ee.translate.keeleleek.mtapplication.common.requests.SpellingSuggestRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import ee.translate.keeleleek.mtpluginframework.spellcheck.SpellerPlugin;

public class SpellerProxy extends Proxy {
	
	public final static String NAME = "{32E280C0-B06D-4A23-B48A-9B8F6EB291F0}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(SpellerProxy.class);
	
	public final static PluginVersion SPELLER_FRAMEWORK_VERSION = new PluginVersion(1, 0);

	private ArrayList<SpellerPlugin> spellerPlugins = new ArrayList<>();
	private ArrayList<PluginVersion> spellerVersions = new ArrayList<>();
	private HashMap<Reference,List<Misspell>> allMisspells = new HashMap<>();
	
	int sid = 0;
	
	
	// INIT
	public SpellerProxy() {
		super(NAME, "");
	}

	void addPlugin(SpellerPlugin plugin, PluginVersion version) {
		spellerPlugins.add(plugin);
		spellerVersions.add(version);
	}
	
	SpellerPlugin loadPlugin(File file, PluginDescription description)
	{
		try {
			// compatible
			PluginVersion frameworkReqVersion = description.getSpellerFrameworkVersion();
			if (!PluginVersion.isFrameworksCompatible(frameworkReqVersion, SPELLER_FRAMEWORK_VERSION)) {
				LOGGER.error("Plugin " + file.getAbsolutePath() + " required framework version " + frameworkReqVersion + " is not compatible with " + SPELLER_FRAMEWORK_VERSION);
				return null;
			};
			
			// version
			PluginVersion version = description.getPluginVersion();
			if (version == null) {
				LOGGER.error("Plugin " + file.getAbsolutePath() + " is missing a version!");
				return null;
			};
			
			// load
			URLClassLoader classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()}, this.getClass().getClassLoader());
			String pluginClassStr = description.getSpellerClass();
			
			Class<?> pluginClass = Class.forName (pluginClassStr, true, classLoader);
			Object instance = pluginClass.newInstance();

			// add
			SpellerPlugin plugin = (SpellerPlugin) instance;
			spellerPlugins.add(plugin);
			spellerVersions.add(version);
			
			LOGGER.info("Successfully loaded " + plugin.getName() + " " + version + " speller plugin from " + file);
			
			return plugin;
			
		} catch (Throwable e) {
			LOGGER.error("Failed to load speller plugin " + file.getAbsolutePath(), e);
		}
		
		return null;
	}

	
	// PLUGINS
	public List<String> getPluginNames(String langCode)
	 {
		ArrayList<String> result = new ArrayList<>();
		for (SpellerPlugin plugin : spellerPlugins) {
			result.add(plugin.getName(langCode));
		}
		return result;
	 }
	
	public List<String> getValidPluginNames(String langCode)
	 {
		ArrayList<String> result = new ArrayList<>();
		for (SpellerPlugin plugin : spellerPlugins) {
			if (plugin instanceof DisabledSpellerPlugin) continue;
			result.add(plugin.getName(langCode));
		}
		return result;
	 }
	
	public SpellerPlugin getPlugin(String pluginName)
	 {
		for (SpellerPlugin plugin : spellerPlugins) {
			if (plugin.getName().equals(pluginName)) return plugin;
		}
		return null;
	 }

	public SpellerPlugin getPlugin(String langCode, String pluginName)
	 {
		for (SpellerPlugin plugin : spellerPlugins) {
			if (plugin.getName(langCode).equals(pluginName)) return plugin;
		}
		return null;
	 }
	
	public PluginVersion getPluginVersion(String langCode, String pluginName)
	 {
		for (int i = 0; i < spellerPlugins.size(); i++) {
			SpellerPlugin plugin = spellerPlugins.get(i);
			if (plugin.getName(langCode).equals(pluginName)) return spellerVersions.get(i);	
		}
		
		return null;
	 }

	
	// SPELLCHECK
	public void requestSpellCheck(SpellCheckRequest request) throws Exception
	 {
		String pluginName = request.getPluginName();
		Reference ref = request.getRef();
		
		if (ref == null) return;
		
		sid++;
		int rsid = sid;
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SPELL_CHECKER_BUSY, true);
		
		MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
		if (article == null) return;
		
		spellCheckArticle(pluginName, article);
		
		if (rsid != sid) return; // another request
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SPELL_CHECKER_BUSY, false);
	 }
	
	public void spellCheckArticle(String pluginName, MinorityArticle article) throws Exception
	 {
		String guiLangCode = MinorityTranslateModel.preferences().getGUILangCode();
		
		SpellerPlugin plugin = getPlugin(guiLangCode, pluginName);
		if (plugin == null) return;

		String langCode = article.getRef().getLangCode();
		
		ArrayList<Misspell> misspells = new ArrayList<>();
		
		boolean success = plugin.spellCheck(langCode, article.getText(), misspells);
		
		if (success) {
			allMisspells.put(article.getRef(), misspells);
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SPELLCHECK_COMPLETE, article.getRef());
		}
	 }

	public List<String> requestSpellingSuggest(SpellingSuggestRequest request) throws Exception
	 {
		String pluginName = request.getPluginName();
		String langCode = request.getLangCode();
		Misspell misspell = request.getMisspell();
		
		return spellingSuggest(pluginName, langCode, misspell);
	 }
	

	public List<String> spellingSuggest(String pluginName, String langCode, Misspell misspell) throws Exception
	 {
		String guiLangCode = MinorityTranslateModel.preferences().getGUILangCode();
		
		SpellerPlugin plugin = getPlugin(guiLangCode, pluginName);
		if (plugin == null) return new ArrayList<>();

		return plugin.findSuggestions(langCode, misspell);
	 }
	
	public List<Misspell> getMisspells(Reference ref)
	 {
		return allMisspells.get(ref);
	 }
	
	
	
}

package ee.translate.keeleleek.mtapplication.controller.gui;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;

public class RestartGUICommand extends SimpleCommand {

	protected static Logger LOGGER = LoggerFactory.getLogger(RestartGUICommand.class);

	
	@Override
	public void execute(INotification notification)
	 {
		LOGGER.info("Restarting application");
		
		// Get window:
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		Object translateWindow = mediator.getWindow();
		
		// Close existing:
		LOGGER.info("Closing existing window");
		sendNotification(Notifications.WINDOW_TRANSLATE_CLOSE);
		
		// Register new mediator:
		LOGGER.info("Initialising new windows");
		mediator = new ApplicationMediatorFX(translateWindow);
		getFacade().registerMediator(mediator);
		
		// Reload session:
		sendNotification(Notifications.SESSION_LOADED);
		
		// Show:
		LOGGER.info("Showing application");
		sendNotification(Notifications.WINDOW_TRANSLATE_OPEN);
	 }
	
}

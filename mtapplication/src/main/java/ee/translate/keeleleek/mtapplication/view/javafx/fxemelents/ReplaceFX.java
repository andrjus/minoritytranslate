package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.ReplaceEntry;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReplaceFX {

	public final StringProperty srcLang;
	public final StringProperty dstLang;
	public final StringProperty find;
	public final StringProperty replace;
	
	
	public ReplaceFX(ReplaceEntry entry)
	 {
		this.srcLang = new SimpleStringProperty(entry.getSrcLang());
		this.dstLang = new SimpleStringProperty(entry.getDstLang());
		this.find = new SimpleStringProperty(entry.getFind());
		this.replace = new SimpleStringProperty(entry.getReplace());
	 }

	public ReplaceEntry toEntry()
	 {
		return new ReplaceEntry(srcLang.getValue(), dstLang.getValue(), find.getValue(), replace.getValue());
	 }
	
	
}

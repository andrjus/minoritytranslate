package ee.translate.keeleleek.mtapplication.model.preferences;

/**
 * Immutable content related preferences.
 *
 */
public class ContentPreferences {

	private Integer categoryDepth;
	
	
	private ContentPreferences() { }
	
	public ContentPreferences(Integer categoryDepth) {
		super();
		this.categoryDepth = categoryDepth;
	}
	
	
	public Integer getCategoryDepth() {
		return categoryDepth;
	}

	public static ContentPreferences create()
	 {
		ContentPreferences preferences = new ContentPreferences();
		
		preferences.categoryDepth = 2;
		
		return preferences;
	 }
	
	
	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categoryDepth == null) ? 0 : categoryDepth.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentPreferences other = (ContentPreferences) obj;
		if (categoryDepth == null) {
			if (other.categoryDepth != null)
				return false;
		} else if (!categoryDepth.equals(other.categoryDepth))
			return false;
		return true;
	}
	
}

package ee.translate.keeleleek.mtapplication.controller.suggestions;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;
import ee.translate.keeleleek.mtapplication.view.suggestions.Suggestable;

public class SuggestCategoryCommand extends SimpleCommand {

	private String langCode = null;
	private String searchTerm = null;
	private Suggestable suggestable = null;

	@Override
	public void execute(INotification notification)
	 {
		this.suggestable = (Suggestable) notification.getBody();
		
		String langCode = notification.getType();
		String searchTerm = suggestable.getSearchTerm();
		
		if (langCode == null || langCode.length() == 0){
			suggestable.suggest(new String[0]);
			return;
		}
		
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
		switch (wikiType) {
		case REGULAR:
			break;
			
		case INCUBATOR:
			String prefix = WikisProxy.prefix(langCode);
			searchTerm = WikisProxy.prefixTitle(searchTerm, prefix);
			break;
			
		default:
			break;
		}
		
		this.searchTerm = searchTerm;
		this.langCode = langCode;
		
		MinorityTranslateModel.bots().requestCategorySuggestions(langCode, searchTerm, this);
	 }
	
	public void handleSuggestions(final String search, final Integer namespace, final String[] results)
	 {
		if (searchTerm.equals(search)) {
			MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
				
				@Override
				public void run() {
					if (suggestable != null) {

						WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
						switch (wikiType) {
						case REGULAR:
							break;
							
						case INCUBATOR:
							String prefix = WikisProxy.prefix(langCode);
							for (int i = 0; i < results.length; i++) {
								results[i] = WikisProxy.deprefixTitle(results[i], prefix);
							}
							break;
							
						default:
							break;
						}
						
						suggestable.suggest(results);
						
					}
				}
				
			});
		}
	 }
	
}

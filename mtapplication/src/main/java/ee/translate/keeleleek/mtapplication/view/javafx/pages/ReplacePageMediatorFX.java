package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.preferences.ReplaceEntry;
import ee.translate.keeleleek.mtapplication.model.preferences.ReplacePreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ReplaceFX;
import ee.translate.keeleleek.mtapplication.view.pages.ReplacePageMediator;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Callback;


public class ReplacePageMediatorFX extends ReplacePageMediator {

	@FXML
	private TableView<ReplaceFX> valuesTable;
	@FXML
	private TableColumn<ReplaceFX, String> findColumn;
	@FXML
	private TableColumn<ReplaceFX, String> replaceColumn;
	@FXML
	private TableColumn<ReplaceFX, String> filterColumn;

	@FXML
	private Button insertButton;
	@FXML
	private Button appendButton;
	@FXML
	private Button moveUpButton;
	@FXML
	private Button moveDownButton;
	@FXML
	private Button removeButton;

	@FXML
	private TextField srcLangEdit;
	@FXML
	private TextField dstLangEdit;
	@FXML
	private TextArea findEdit;
	@FXML
	private TextArea replaceEdit;
	
	
	// IMPLEMENT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		findColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReplaceFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<ReplaceFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(mapping.getValue().find);
				return prop;
			}
		});
		
		replaceColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReplaceFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<ReplaceFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(mapping.getValue().replace);
				return prop;
			}
		});

		filterColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReplaceFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<ReplaceFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(Bindings.concat(mapping.getValue().srcLang, " \u2192 ", mapping.getValue().dstLang));
				return prop;
			}
		});
		
		valuesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ReplaceFX>() {
			@Override
			public void changed(ObservableValue<? extends ReplaceFX> obs, ReplaceFX oldVal, ReplaceFX newVal)
			 {
				if (oldVal != null) {
					oldVal.srcLang.unbind();
					oldVal.dstLang.unbind();
					oldVal.find.unbind();
					oldVal.replace.unbind();
				}
				if (newVal != null) {

					srcLangEdit.setText(newVal.srcLang.getValue());
					newVal.srcLang.bind(srcLangEdit.textProperty());

					dstLangEdit.setText(newVal.dstLang.getValue());
					newVal.dstLang.bind(dstLangEdit.textProperty());

					findEdit.setText(newVal.find.getValue());
					newVal.find.bind(findEdit.textProperty());

					replaceEdit.setText(newVal.replace.getValue());
					newVal.replace.bind(replaceEdit.textProperty());
					
				}
			 }
		});
	
		valuesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ReplaceFX>() {
			@Override
			public void changed(ObservableValue<? extends ReplaceFX> obs, ReplaceFX oldVal, ReplaceFX newVal)
			 {
				appendButton.setDisable(false);
				insertButton.setDisable(false);
				moveUpButton.setDisable(newVal == null);
				moveUpButton.setDisable(newVal == null);
				moveDownButton.setDisable(newVal == null);
				removeButton.setDisable(newVal == null);
				srcLangEdit.setDisable(newVal == null);
				dstLangEdit.setDisable(newVal == null);
				findEdit.setDisable(newVal == null);
				replaceEdit.setDisable(newVal == null);
				if (newVal == null) {
					srcLangEdit.setText("");
					dstLangEdit.setText("");
					findEdit.setText("");
					replaceEdit.setText("");
				}
			 }
		});
		
	 }

	
	// PAGE
	@Override
	public void open(ReplacePreferences preferences)
	 {
		ObservableList<ReplaceFX> mappingListFX = FXCollections.observableArrayList();
		for (int i = 0; i < preferences.getReplaceCount(); i++) {
			mappingListFX.add(new ReplaceFX(preferences.getReplace(i)));
		}
		
		valuesTable.setItems(mappingListFX);

		srcLangEdit.setText("");
		dstLangEdit.setText("");
		findEdit.setText("");
		replaceEdit.setText("");
		
		valuesTable.getSelectionModel().selectFirst();
	 }
	
	@Override
	public ReplacePreferences close()
	 {
		ObservableList<ReplaceFX> mappingListFX = valuesTable.getItems();
		ArrayList<ReplaceEntry> mappingList = new ArrayList<>(mappingListFX.size());
		for (ReplaceFX mappingFX : mappingListFX) {
			mappingList.add(mappingFX.toEntry());
		}
		
		return new ReplacePreferences(mappingList);
	 }
	
	
	// TABLE
	private void addRow(int index)
	 {
		ObservableList<ReplaceFX> items = valuesTable.getItems();
		if (index > items.size()) index = items.size();
		if (index < 0) index = 0;
		
		items.add(index, new ReplaceFX(new ReplaceEntry()));
		valuesTable.getSelectionModel().select(items.get(index));
	 }
	
	private void removeRow(int index)
	 {
		ObservableList<ReplaceFX> items = valuesTable.getItems();
		if (index >= items.size()) return;
		if (index < 0) return;

		boolean select = valuesTable.getSelectionModel().getSelectedIndex() != -1;
		
		items.remove(index);
		
		if (select) {
			if (index == items.size()) index--;
			valuesTable.getSelectionModel().select(index);
		}
	 }

	private void moveRowDown(int index)
	 {
		ObservableList<ReplaceFX> items = valuesTable.getItems();
		if (index >= items.size() - 1) return;
		if (index < 0) return;

		boolean select = valuesTable.getSelectionModel().getSelectedIndex() != -1;
		
		ReplaceFX item = items.remove(index);
		items.add(index + 1, item);

		if (select) {
			valuesTable.getSelectionModel().select(item);
		}
	 }

	private void moveRowUp(int index)
	 {
		ObservableList<ReplaceFX> items = valuesTable.getItems();
		if (index >= items.size()) return;
		if (index < 1) return;

		boolean select = valuesTable.getSelectionModel().getSelectedIndex() != -1;
		
		ReplaceFX item = items.remove(index);
		items.add(index - 1, item);

		if (select) {
			valuesTable.getSelectionModel().select(item);
		}
	 }
	
	
	// ACTIONS
	@FXML
	private void onAppendClick() {
		addRow(valuesTable.getSelectionModel().getSelectedIndex() + 1);
	}
	
	@FXML
	private void onInsertClick() {
		addRow(valuesTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveUpClick() {
		moveRowUp(valuesTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveDownClick() {
		moveRowDown(valuesTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onRemoveClick() {
		removeRow(valuesTable.getSelectionModel().getSelectedIndex());
	}

	
}

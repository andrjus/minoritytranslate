package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.PullingPreferences;


public abstract class PullingPageMediator extends Mediator implements PreferencesPage<PullingPreferences> {

	public final static String NAME = "{C7C32064-CDD8-499F-86C6-13C0A2F5B23B}";
	
	
	// INIT
	public PullingPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}

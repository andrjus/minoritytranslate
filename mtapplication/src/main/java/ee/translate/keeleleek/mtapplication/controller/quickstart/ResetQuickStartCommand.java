package ee.translate.keeleleek.mtapplication.controller.quickstart;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class ResetQuickStartCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		MinorityTranslateModel.preferences().resetQuickStart();
	 }

}

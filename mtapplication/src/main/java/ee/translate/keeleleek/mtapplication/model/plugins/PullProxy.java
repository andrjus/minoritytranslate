package ee.translate.keeleleek.mtapplication.model.plugins;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.PullRequest;
import ee.translate.keeleleek.mtapplication.common.requests.PullResponse;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMapping;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.chopup.ArticleChopup;
import ee.translate.keeleleek.mtpluginframework.chopup.RootElement;
import ee.translate.keeleleek.mtpluginframework.mapping.PullTemplateParameterMapping;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;
import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;

public class PullProxy extends Proxy {
	
	public final static String NAME = "{C30E5969-7DE3-499F-8CE2-A6BA9E7C73BD}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(PullProxy.class);
	
	public final static PluginVersion PULL_FRAMEWORK_VERSION = new PluginVersion(1, 1);
	
	private ArrayList<PullPlugin> pullPlugins = new ArrayList<>();
	private ArrayList<PluginVersion> pullVersions = new ArrayList<>();
	
	int tid = 0;
	
	
	// INIT
	public PullProxy() {
		super(NAME, "");
	}
	
	void addPlugin(PullPlugin plugin, PluginVersion version) {
		pullPlugins.add(plugin);
		pullVersions.add(version);
	}
	
	PullPlugin loadPlugin(File file, PluginDescription description)
	{
		try {
			// compatible
			PluginVersion frameworkReqVersion = description.getPullFrameworkVersion();
			if (!PluginVersion.isFrameworksCompatible(frameworkReqVersion, PULL_FRAMEWORK_VERSION)) {
				LOGGER.error("Plugin " + file.getAbsolutePath() + " required framework version " + frameworkReqVersion + " is not compatible with " + PULL_FRAMEWORK_VERSION);
				return null;
			};

			// version
			PluginVersion version = description.getPluginVersion();
			if (version == null) {
				LOGGER.error("Plugin " + file.getAbsolutePath() + " is missing a version!");
				return null;
			};
			
			// load
			URLClassLoader classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()}, this.getClass().getClassLoader());
			String pluginClassStr = description.getPullClass();
			
			Class<?> pluginClass = Class.forName (pluginClassStr, true, classLoader);
			Object instance = pluginClass.newInstance();

			// add
			PullPlugin plugin = (PullPlugin) instance;
			pullPlugins.add(plugin);
			pullVersions.add(version);
					
			LOGGER.info("Successfully loaded " + plugin.getName() + " " + version + " pull plugin from " + file);
			
			return plugin;
			
		} catch (Throwable e) {
			LOGGER.error("Failed to load pull plugin " + file.getAbsolutePath(), e);
		}
		
		return null;
	}

	
	// PLUGINS
	public List<String> getPluginNames(String langCode)
	 {
		ArrayList<String> result = new ArrayList<>();
		for (PullPlugin plugin : pullPlugins) {
			result.add(plugin.getName(langCode));
		}
		return result;
	 }

	public PullPlugin getPlugin(String pluginName)
	 {
		for (PullPlugin plugin : pullPlugins) {
			if (plugin.getName().equals(pluginName)) return plugin;
		}
		return null;
	 }

	public PullPlugin getPlugin(String langCode, String pluginName)
	 {
		for (PullPlugin plugin : pullPlugins) {
			if (plugin.getName(langCode).equals(pluginName)) return plugin;
		}
		return null;
	 }
	
	public PluginVersion getPluginVersion(String langCode, String pluginName)
	 {
		for (int i = 0; i < pullPlugins.size(); i++) {
			PullPlugin plugin = pullPlugins.get(i);
			if (plugin.getName(langCode).equals(pluginName)) return pullVersions.get(i);	
		}
		
		return null;
	 }

	
	// PULLING
	public void requestPull(PullRequest request, PullCallback progress) throws Exception
	 {
		final String pluginName = request.getPluginName();
		final Reference srcRef = request.getSrcRef();
		final Reference dstRef = request.getDstRef();
		
		tid++;
		int rtid = tid;
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.PULLING_BUSY, true);
		
		MinorityArticle srcArticle = MinorityTranslateModel.content().getArticle(srcRef);
		if (srcArticle == null) return;
		
		MinorityArticle dstArticle = MinorityTranslateModel.content().getArticle(dstRef);
		if (dstArticle == null) return;
		
		try {
			pullArticle(pluginName, srcArticle, dstArticle, progress);
		} finally {
			if (rtid != tid) return; // another request
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.PULLING_BUSY, false);
		}
	 }

	public void pullArticle(String pluginName, MinorityArticle srcArticle, MinorityArticle dstArticle, PullCallback progress) throws Exception
	 {
		String guiLangCode = MinorityTranslateModel.preferences().getGUILangCode();
		
		PullPlugin plugin = getPlugin(guiLangCode, pluginName);
		if (plugin == null) return;

		String srcLangCode = srcArticle.getRef().getLangCode();
		String dstLangCode = dstArticle.getRef().getLangCode();
		
		progress.action(Messages.getString("plugins.pull.preparing"));
		
		// process
		String srcText = MinorityTranslateModel.processer().collectReplaceProcess(srcLangCode, dstLangCode, srcArticle.getTitle(), srcArticle.getFilteredText(), dstArticle.getTitle(), dstArticle.getText());
		
		// chopup
		RootElement srcRoot = ArticleChopup.chopup(srcText);
		
		// pull
		boolean success = plugin.pull(srcLangCode, dstLangCode, srcArticle.getTitle(), srcRoot, dstArticle.getTitle(), dstArticle.getText(), progress);
		
		progress.finish(success);
		
		try {
			while (!progress.isResolved()) {
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (success && progress.isConfirmed()) {
			progress.advance();
			if (progress.isSaveTemplateMappings()) saveMappings(srcLangCode, dstLangCode, progress.getTemplateParameterMappings());
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.PULL_COMPLETE, new PullResponse(new Reference(srcArticle.getRef().getQid(), dstLangCode), srcRoot.toCode()));
		}
	 }

	
	// SAVING
	private void saveMappings(String srcLangCode, String dstLangCode, List<PullTemplateParameterMapping> pullMappings)
	 {
		// convert
		TemplateMappingPreferences preferences = MinorityTranslateModel.preferences().getTemplateMapping();
		ArrayList<TemplateMapping> mappings = new ArrayList<>();
		
		for (int i = 0; i < preferences.getMappingCount(); i++) {
			mappings.add(preferences.getMapping(i));
		}
		
		// create new ones
		for (PullTemplateParameterMapping pullMapping : pullMappings) {
			
			if (pullMapping.isOriginal()) continue; // no changes
			
			// create mapping
			String templateName = pullMapping.getTemplateName();
			String parameterName = pullMapping.getParameterName();
			String remap = pullMapping.getDestination();
			TemplateMapping mapping = new TemplateMapping(templateName, parameterName, srcLangCode, remap, dstLangCode);
			
			// add
			int i = indexOf(srcLangCode, dstLangCode, templateName, parameterName, mappings);
			if (i == -1) {
				mappings.add(mapping);
			} else {
				mappings.set(i, mapping);
			}
			
		}
		
		//preferences
		preferences = new TemplateMappingPreferences(mappings);
		sendNotification(Notifications.PREFERENCES_CHANGE_TEMPLATE_MAPPING, preferences);
	 }
	
	private int indexOf(String srcLangCode, String dstLangCode, String templateName, String parameterName, List<TemplateMapping> mappings)
	 {
		for (int i = 0; i < mappings.size(); i++) {
			TemplateMapping mapping = mappings.get(i);
			if (!mapping.getFilter().getSrcLangRegex().equals(srcLangCode)) continue;
			if (!mapping.getFilter().getDstLangRegex().equals(dstLangCode)) continue;
			if (mapping.getSrcTemplate().equals(templateName) && mapping.getSrcParameter().equals(parameterName)) return i;
		}
		return -1;
	 }
	
	
}

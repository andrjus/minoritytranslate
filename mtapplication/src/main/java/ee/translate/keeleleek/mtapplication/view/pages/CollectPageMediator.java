package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.CollectPreferences;


public abstract class CollectPageMediator extends Mediator implements PreferencesPage<CollectPreferences> {

	public final static String NAME = "{4A594E06-C6D9-4F97-B4DE-EB6A4652138B}";
	
	
	// INIT
	public CollectPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}

package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import javafx.beans.property.StringProperty;

public interface SuggestionListFX {

	public StringProperty name();
	
	public String getFullName();
	
}

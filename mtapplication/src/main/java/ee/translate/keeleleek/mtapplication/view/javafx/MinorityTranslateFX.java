package ee.translate.keeleleek.mtapplication.view.javafx;

import ee.translate.keeleleek.mtapplication.MinorityTranslateFacade;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MinorityTranslateFX extends Application {

	private Pane splashRoot;
	private Image splashIcon;
	private ImageView splashLogo;
	
	private Stage splashStage;
	private Stage mainStage;

	public static void main(String[] args) throws Exception
	 {
		launch(args);
	 }

	@Override
	public void init()
	 {
		splashIcon = new Image("icon.png");
		splashLogo = new ImageView(new Image("logo.png"));
		splashRoot = new VBox();
		splashRoot.getChildren().addAll(splashLogo);
	 }

	@Override
	public void start(Stage stage) throws Exception
	 {
		this.mainStage = stage;
		showSplash();
		
		showMainStage();
	 }

	private void showMainStage() throws Exception
	 {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				final MinorityTranslateFacade facade = new MinorityTranslateFacade();
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						facade.launch(mainStage);
						splashStage.close();
						mainStage.show();
						facade.done();
					}
				});
			}
		});
		thread.start();
	 }

	private void showSplash()
	 {
		Scene splashScene = new Scene(splashRoot);
		splashScene.setFill(Color.TRANSPARENT);
		splashStage = new Stage();
		splashStage.initStyle(StageStyle.TRANSPARENT);
		splashStage.setTitle("Minority Translate");
		splashStage.getIcons().add(splashIcon);
		splashStage.setScene(splashScene);
		splashStage.centerOnScreen();
		splashStage.initModality(Modality.APPLICATION_MODAL);
		splashStage.initOwner(mainStage);
		splashStage.show();
	 }
	
	
}

package ee.translate.keeleleek.mtapplication.view.views;

import java.util.ArrayList;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorMode;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorPosition;

public abstract class ViewMediator extends Mediator {

	// constants
	public final static String NAME = "{D3E1230C-C5A4-4CBC-A325-D68B24BDF519}";
	
	public enum ViewMode { SPLIT_VERTICAL, SPLIT_HORIZONTAL, ALIGN, SINGLE };
	
	private ViewMode viewMode;
	
	// logger
	//private final static Logger LOGGER = LoggerFactory.getLogger(ViewMediator.class);
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public ViewMediator(ViewMode viewMode)
	 {
		super(NAME, null);
		this.viewMode = viewMode;
	 }
	
	@Override
	public void onRemove()
	 {
		// unregister mediators
		List<EditorMediator> editors;
		editors = getEditors(EditorPosition.DESTINATION);
		for (EditorMediator editorMediator : editors) {
			getFacade().removeMediator(editorMediator.getMediatorName());
		}

		editors = getEditors(EditorPosition.SOURCE);
		for (EditorMediator editorMediator : editors) {
			getFacade().removeMediator(editorMediator.getMediatorName());
		}
	 }
	

	/* ******************
	 *                  *
	 *   Notification   *
	 *                  *
	 ****************** */
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.SESSION_LOADED,
			Notifications.ENGINE_SELECTED_ARTICLE_CHANGED,
			Notifications.ENGINE_ARTICLE_STATUS_CHANGED
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {

		case Notifications.SESSION_LOADED:
			refreshEditorMarks();
			break;
			
		case Notifications.ENGINE_SELECTED_ARTICLE_CHANGED:
			refreshEditorMarks();
			onSelectedArticleChanged();
			break;

		case Notifications.ENGINE_ARTICLE_STATUS_CHANGED:
			refreshEditorMarks();
			break;
			
		default:
			break;
		}
	 }
	
	

	/* ******************
	 *                  *
	 *     Refresh      *
	 *                  *
	 ****************** */
	private void refreshEditorMarks()
	 {
		refreshEditorMarks(EditorPosition.SOURCE);
		refreshEditorMarks(EditorPosition.DESTINATION);
	 }
	
	private void refreshEditorMarks(EditorPosition pos)
	 {
		int numEditors = getEditorCount(pos);
		for (int i = 0; i < numEditors; i++) {
			boolean isNew = getEditor(pos, i).isArticleNew();
			markEditorNew(pos, i, isNew);
		}
	 }
	
	

	/* ******************
	 *                  *
	 *       View       *
	 *                  *
	 ****************** */
	public ViewMode getViewMode() {
		return viewMode;
	}
	
	

	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	public List<String> langCodesFor(EditorPosition pos)
	 {
		List<String> langCodes;
		if (pos == EditorPosition.SOURCE) langCodes = MinorityTranslateModel.preferences().languages().filterSrcLangCodes();
		else if (pos == EditorPosition.DESTINATION) langCodes = MinorityTranslateModel.preferences().languages().filterDstLangCodes();
		else langCodes = new ArrayList<>();
		return langCodes;
	 }
	
	public abstract String findSelectedSrcLangCode();
	
	
	/* ******************
	 *                  *
	 *     Editors      *
	 *                  *
	 ****************** */
	public abstract EditorMediator createEditor(EditorMode mode, EditorPosition pos);
	public abstract EditorMediator destroyEditor(EditorPosition pos);
	public abstract EditorMediator getEditor(EditorPosition pos, int i);
	public abstract int getEditorCount(EditorPosition pos);
	public abstract List<EditorMediator> getEditors(EditorPosition pos);
	public abstract EditorMediator findSelectedEditor(EditorPosition box);
	public abstract void commitEditors();
	public abstract void setEditorName(EditorPosition pos, int i, String name);
	public abstract void markEditorNew(EditorPosition pos, int i, boolean isNew);
	
	
	/* ******************
	 *                  *
	 *     Events       *
	 *                  *
	 ****************** */
	protected void onSelectedArticleChanged() { }
	
	
}

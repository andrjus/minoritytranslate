package ee.translate.keeleleek.mtapplication.common.requests;

import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;

public class SpellingSuggestRequest {

	private String pluginName;
	private String langCode;
	private Misspell misspell;
	
	
	public SpellingSuggestRequest(String pluginName, String langCode, Misspell misspell) {
		super();
		this.pluginName = pluginName;
		this.langCode = langCode;
		this.misspell = misspell;
	}
	

	public String getPluginName() {
		return pluginName;
	}
	
	public String getLangCode() {
		return langCode;
	}
	
	public Misspell getMisspell() {
		return misspell;
	}
	
	
}

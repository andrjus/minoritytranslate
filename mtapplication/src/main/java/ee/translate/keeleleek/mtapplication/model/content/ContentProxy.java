package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.puremvc.java.multicore.patterns.facade.Facade;
import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Articon;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;

public class ContentProxy extends Proxy {

	public final static String NAME = "{D6829DFF-241E-4B05-833E-B13000F09D65}";

	private static Logger LOGGER = LoggerFactory.getLogger(ContentProxy.class);
	
	public final static long TIMEOUT = 250*0 + 1000;
	
	private HashMap<Reference, MinorityArticle> articles = new HashMap<>();
	
	private String selectedQid = null;
	
	private CopyOnWriteArrayList<String> qids = new CopyOnWriteArrayList<>();
	private ArrayList<String> selectable = new ArrayList<>();
	private HashMap<String,String> selectableTitles = new HashMap<>();
	
	private ContentQueuerProxy queuer = new ContentQueuerProxy();
	private ContentDownloaderProxy downloader = new ContentDownloaderProxy();
	private ContentPreviewerProxy previewer = new ContentPreviewerProxy();
	private ContentUploaderProxy uploader = new ContentUploaderProxy();
	private ContentTasksProxy tasks = new ContentTasksProxy();
	
	private HashMap<String, QidMeta> qidMetas = new HashMap<>();
	
	
	// INIT
	public ContentProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		kickstart();
		
		Facade facade = getFacade();
		
		facade.registerProxy(queuer);
		facade.registerProxy(downloader);
		facade.registerProxy(previewer);
		facade.registerProxy(uploader);
		facade.registerProxy(tasks);
		
	 }
	
	@Override
	public void onRemove()
	 {
		Facade facade = getFacade();
		
		facade.removeMediator(queuer.getProxyName());
		facade.removeMediator(downloader.getProxyName());
		facade.removeMediator(previewer.getProxyName());
		facade.removeMediator(uploader.getProxyName());
		facade.removeProxy(tasks.getProxyName());
	 }
	
	
	// CONTENT INIT
	public void initDownload(Reference ref, String rev, String title, String text)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to initialise article because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		if (article.getStatus() != Status.DOWNLOAD_IN_PROGRESS) {
			LOGGER.error("Failed to initialise article because the article was not downloading!" + article.getStatus());
			return;
		}

		article.setPreviousRevision(rev);
		article.setTitle(title);
		article.setText(text);
		article.setOriginal();
		article.setStatus(Status.STANDBY);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_TITLE_CHANGED, ref);
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_TEXT_CHANGED, ref);
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_STATUS_CHANGED, ref);
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_DOWNLOAD_INITIALISED, ref);
	 }
	
	
	// CONTENT PURGE
	public void purgeDownloading()
	 {
		ArrayList<String> qids = new ArrayList<>(this.qids);
		for (String qid : qids) {
			if (hasStatus(qid, Status.DOWNLOAD_REQUESTED) || hasStatus(qid, Status.DOWNLOAD_IN_PROGRESS)) removeQid(qid);
		}
	 }
	
	
	// CONTENT META
	public void updateQidMeta(QidMeta qidMeta) {
		this.qidMetas.put(qidMeta.getQid(), qidMeta);
	}
	
	public QidMeta getQidMeta(String qid) {
		return qidMetas.get(qid);
	}
	
	
	// CONTROL
	public void kickstart()
	 {
		Set<Reference> references = articles.keySet();
		for (Reference ref : references) {
			
			MinorityArticle article = articles.get(ref);
			if (article == null) continue;
			
			switch (article.getStatus()) {
			case DOWNLOAD_IN_PROGRESS:
				article.setStatus(Status.DOWNLOAD_REQUESTED);
				break;
				
			case UPLOAD_PREPARING:
			case UPLOAD_IN_PROGRESS:
			case UPLOAD_CONFIRMING:
			case LINKING_ARTICLES:
			case SENDING_USAGE_INFO:
				article.setStatus(Status.STANDBY);
				break;

			case UPLOAD_CONFLICT:
			case UPLOAD_FAILED:
			default:
				break;
			}

		}
		
		queuer.cancel();
		downloader.cancel();
	 }
	
	public void cancel()
	 {
		queuer.cancel();
		downloader.cancel();
//		previewer.cancel();
//		uploader.cancel();
	 }
	
	
	// INTERNAL
	Collection<Reference> getReferences() {
		return articles.keySet();
	}

	Collection<MinorityArticle> getArticles() {
		return articles.values();
	}
	
	List<String> getQids() {
		return qids;
	}

	
	// ARTICLE VALUES
	public boolean hasArticle(Reference ref)
	 {
		return articles.containsKey(ref);
	 }

	public void createArticle(WikiReference request, Reference ref, String title)
	 {
		MinorityArticle article = new MinorityArticle(request, ref, title, "");
		
		LOGGER.info("Creating article " + article);
		articles.put(article.getRef(), article);
		
		String qid = article.getRef().getQid();
		if (!qids.contains(qid)) qids.add(qid);
		
		article.setExact(MinorityTranslateModel.preferences().corpus().isExact());
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_ADDED, article.getRef());

		updateSelectable();
	 }
	

	public MinorityArticle getArticle(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		
		return article;
	 }

	public Status getStatus(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getStatus();
	 }

	public TitleStatus getTitleStatus(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getTitleStatus();
	 }

	public String getTitle(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getTitle();
	 }

	public String getText(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getText();
	 }

	public String getFilteredText(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getFilteredText();
	 }

	public String getPreview(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getPreview();
	 }

	public String getFilteredPreview(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return null;
		
		return article.getFilteredPreview();
	 }

	public boolean isExact(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		if (article == null) return false;
		
		return article.isExact();
	 }

	
	public boolean hasStatus(String qid, Status status)
	 {
		List<String> langCodes = MinorityTranslateModel.preferences().languages().getAllLangCodes();
		for (String langCode : langCodes) {
			Reference ref = new Reference(qid, langCode);
			MinorityArticle article = getArticle(ref);
			if (article != null && article.getStatus() == status) return true;
		}
		
		return false;
	 }
	
	public boolean hasArticon(String qid, Articon articon)
	 {
		List<String> langCodes = MinorityTranslateModel.preferences().languages().filterDstLangCodes();
		for (String langCode : langCodes) {
			Reference ref = new Reference(qid, langCode);
			MinorityArticle article = getArticle(ref);
			if (article != null && article.findArticon() == articon) return true;
		}
		
		return false;
	 }
	

	// ARTICLE CHANGING
	public void changeStatus(Reference ref, Status status)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article status because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setStatus(status);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_STATUS_CHANGED, ref);
	 }
	
	public void changeTitleStatus(Reference ref, TitleStatus status)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article title status because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setTitleStatus(status);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED, ref);
	 }

	public void changeTitle(Reference ref, String text)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article title because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setTitle(text);
		
		if (article.isNew()) {
			article.setTitleStatus(TitleStatus.CHECKING_REQUIRED);
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED, ref);
		}
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_TITLE_CHANGED, ref);
		
		updateSelectable();
		
		MinorityTranslateModel.timer().scheduleAutosave();
	 }

	public void changeText(Reference ref, String text)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article text because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setText(text);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_TEXT_CHANGED, ref);

		MinorityTranslateModel.timer().scheduleAutosave();
	 }

	public void changeExact(Reference ref, boolean exact)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article exact translation because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setExact(exact);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_EXACT_CHANGED, ref);
	 }

	public void requestPreview(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to request article preview because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.requestPreview();
	 }

	public void changePreview(Reference ref, String source, String preview)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article preview because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setPreview(source, preview);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_PREVIEW_CHANGED, ref);
	 }

	public void requestFilteredPreview(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to request article preview because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.requestFilteredPreview();
	 }

	public void changeFilteredPreview(Reference ref, String source, String preview)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article preview because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setFilteredPreview(source, preview);
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_FILTERED_PREVIEW_CHANGED, ref);
	 }
	

	public void requestUpload(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);

		if (article == null) {
			LOGGER.error("Failed to request article upload because article with reference " + ref + " doesnt exist!");
			return;
		}

		System.out.println(article.isUploadRequired());
		
		if (!article.isUploadRequired()) return;
		
		if (article.getTitle().isEmpty()) return;

		article.requestUpload();
		
		if (article.getStatus() != Status.UPLOAD_REQUESTED) {
			LOGGER.error("Failed to request article upload!");
			return;
		}
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_STATUS_CHANGED, ref);
	 }
	
	public void cancelRequestUpload(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);

		if (article == null) {
			LOGGER.error("Failed to cancel article upload request because article with reference " + ref + " does not exist!");
			return;
		}

		article.cancelRequestUpload();
		
		if (article.getStatus() != Status.STANDBY) {
			LOGGER.error("Failed to cancel upload request!");
			return;
		}
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_STATUS_CHANGED, ref);
	 }
	
	
	void setOriginal(Reference ref)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to set to original state because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setOriginal();
	 }


	public boolean isSaved()
	 {
		Collection<MinorityArticle> articles = getArticles();
		for (MinorityArticle article : articles) {
			if (!article.isSaved()) return false;
		}
		
		return true;
	 }
	
	public void setSaved()
	 {
		Collection<MinorityArticle> articles = getArticles();
		for (MinorityArticle article : articles) {
			article.setSaved();
		}
	 }

	public void changeSrcLangCode(Reference ref, String langCode)
	 {
		MinorityArticle article = articles.get(ref);
		
		if (article == null) {
			LOGGER.error("Failed to change article source language code because article with reference " + ref + " doesnt exist!");
			return;
		}
		
		article.setSrcLangCode(langCode);

		MinorityTranslateModel.timer().scheduleAutosave();
	 }

	
	// SELECTION
	public String getSelectedQid() {
		return selectedQid;
	}

	public void changeSelectedQid(String qid)
	 {
		if (qid == null && this.selectedQid == null) return;
		if (qid != null && qid.equals(this.selectedQid)) return;

		this.selectedQid = qid;

		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_SELECTED_ARTICLE_CHANGED, qid);
	 }

	public void changeNextQid()
	 {
		if (selectable.size() == 0) {
			changeSelectedQid(null);
			return;
		}
		
		String qid = this.selectedQid;
		int i = selectable.indexOf(qid);
		
		if (i == -1) {
			changeSelectedQid(selectable.get(0));
			return;
		}
		
		i++;
		if (i >= selectable.size()) i = 0;
		
		changeSelectedQid(selectable.get(i));
	 }

	public void changePreviousQid()
	 {
		if (selectable.size() == 0) {
			changeSelectedQid(null);
			return;
		}
		
		String qid = this.selectedQid;
		int i = selectable.indexOf(qid);
		
		if (i == -1) {
			changeSelectedQid(selectable.get(0));
			return;
		}
		
		i--;
		if (i < 0) i = selectable.size() - 1;
		
		changeSelectedQid(selectable.get(i));
	 }
	
	public void removeQid(String qid)
	 {
		if (qid.equals(selectedQid)) {
			if (qids.size() <= 1) changeSelectedQid(null);
			else if (qids.get(qids.size() - 1).equals(qid)) changePreviousQid();
			else changeNextQid();
		}
		
		Set<Reference> refs = ContentUtil.extractReferences(this, qid);
		for (Reference reference : refs) {
			articles.remove(reference);
		}
		
		if (qids.remove(qid)) updateSelectable();
	 }
	
	
	// SELECTABLE
	synchronized private void updateSelectable()
	 {
		// save and clear old selectables
		ArrayList<String> origSelectable = new ArrayList<>(selectable);
		HashMap<String,String> origSelectableTitles = new HashMap<>(selectableTitles);
		
		selectable.clear();
		selectableTitles.clear();
		
		// sort qids
		Collections.sort(this.qids, new Comparator<String>() {
			@Override
			public int compare(String qid1, String qid2) {
				String title1 = findSelectionItem(qid1);
				String title2 = findSelectionItem(qid2);
				return title1.compareTo(title2);
			}
		});
		
		// new selectables
		for (String qid : this.qids) {
			
			if (selectable.contains(qid)) continue;
			
			selectable.add(qid);
			selectableTitles.put(qid, findSelectionItem(qid));
			
		}
		
		// notify if changed
		if (!selectable.equals(origSelectable) || !selectableTitles.equals(origSelectableTitles)) MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_SELECTABLE_ITEMS_CHANGED, compileSelectionItems());
		
		if (selectedQid == null && selectable.size() > 0) changeSelectedQid(selectable.get(0));
		
//		// select new
//		if (selectable.size() == 0) changeSelectedQid(null);
//		else if (selectedQid == null) changeSelectedQid(selectable.get(0));
//		else if (selectable.contains(selectedQid)) changeSelectedQid(selectable.get(0));
//		else changeSelectedQid(selectable.get(0));
	 }
	
	
	public String[] compileSelectionItems()
	 {
		String[] result = new String[selectable.size()];
		
		for (int i = 0; i < result.length; i++) {
			
			result[i] = findSelectionItem(selectable.get(i));
			
		}
		
		return result;
	 }

	private String findSelectionItem(String qid)
	 {
		List<String> langCodes = MinorityTranslateModel.preferences().languages().getAllLangCodes();
		
		String title = qid;
		for (String langCode : langCodes) {
			
			Reference ref = new Reference(qid, langCode);
			MinorityArticle article = getArticle(ref);
			if (article == null) continue;
			if (!article.getTitle().isEmpty()) {
				title = article.getTitle();
				break;
			}
			
		}

		return title;
	 }

	public int findSelecedIndex() {
		if (selectedQid == null) return -1;
		return selectable.indexOf(selectedQid);
	}

	public int findIndex(String qid) {
		if (qid == null) return -1;
		return selectable.indexOf(qid);
	}
	
	public String findQid(int i) {
		if (i < 0 || i >= selectable.size()) return null;
		return selectable.get(i);
	}

	public String findQid(String langCode, String title)
	 {
		Collection<MinorityArticle> articles = getArticles();
		for (MinorityArticle article : articles) {
			if (article.getRef().getLangCode().equals(langCode) && article.getTitle().equals(title)) return article.getRef().getQid();
		}
		
		return null;
	 }
	
	public int getSelectableCount() {
		return selectable.size();
	}
	
	public Articon findArticlon(int i)
	 {
		String qid = qids.get(i);

		if (hasArticon(qid, Articon.PROBLEM)) return Articon.PROBLEM;
		if (hasArticon(qid, Articon.BUSY)) return Articon.BUSY;
		if (hasArticon(qid, Articon.EDITED)) return Articon.EDITED;
		if (hasArticon(qid, Articon.NEW)) return Articon.NEW;
		if (hasArticon(qid, Articon.EXISTS)) return Articon.EXISTS;
		return Articon.UNKNOWN;
	 }
	
	
	// COUNTING
	public int countDstArticles() {
		return ContentUtil.countLangCodes(this, MinorityTranslateModel.preferences().languages().filterDstLangCodes());
	}
	
	public int countDoneDstArticles() {
		return ContentUtil.countLangCodesDone(this, MinorityTranslateModel.preferences().languages().filterDstLangCodes());
	}
	
	
}

package ee.translate.keeleleek.mtapplication.model.lists;

public class QueryPageList implements SuggestionList {

	public final static String GROUP_QUERYPAGE = "Querypage";
	
	private String langCode;
	private String title;
	
	
	public QueryPageList(String langCode, String listName) {
		this.langCode = langCode;
		this.title = listName;
	}
	
	
	public String getLangCode() {
		return langCode;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getName() {
		return title + " (" + langCode + ")";
	}
	
	@Override
	public String getGroup() {
		return GROUP_QUERYPAGE;
	}
	
	
}

package ee.translate.keeleleek.mtapplication.controller.wikis;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class FetchWikisCommand extends SimpleCommand {

	public final static Path WIKIS_PATH = FileSystems.getDefault().getPath(MinorityTranslate.ROOT_PATH, "wikis.json");
	public final static String JAR_PATH = "/wikis.json";
	
	private static Logger LOGGER = LoggerFactory.getLogger(FetchWikisCommand.class);

	
	@Override
	public void execute(INotification notification)
	 {
		LOGGER.info("Reading wikis");
		
		String json;
		try {
			
			if (WIKIS_PATH.toFile().isFile()) {
				json = FileUtil.read(WIKIS_PATH);
			} else {
				json = FileUtil.readFromJar(JAR_PATH);
			}
			
		} catch (IOException e) {
			LOGGER.error("Failed to read wikis", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.wikis.load.failed"), Messages.getString("messages.wikis.load.failed.to").replaceFirst("#", WIKIS_PATH.toString()).replaceFirst("#",e.getMessage()));
			return;
		}
		
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
		builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
		Gson gson = builder.create();
		
		WikisProxy wikisProxy = gson.fromJson(json, WikisProxy.class);

		getFacade().registerProxy(wikisProxy);
	 }
	
}

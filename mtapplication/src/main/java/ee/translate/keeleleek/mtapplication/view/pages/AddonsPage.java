package ee.translate.keeleleek.mtapplication.view.pages;

public interface AddonsPage {

	public void open();
	public void close();
	
}

package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;

public class OpenSessionSaveAsCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		sendNotification(Notifications.WINDOW_SAVE_SESSION_OPEN);
	 }
	
}

package ee.translate.keeleleek.mtapplication.common.requests;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class SpellCheckRequest {

	private String pluginName;
	private Reference ref;
	
	
	public SpellCheckRequest(String pluginName, Reference ref) {
		super();
		this.pluginName = pluginName;
		this.ref = ref;
	}
	

	public String getPluginName() {
		return pluginName;
	}
	
	public Reference getRef() {
		return ref;
	}
	
	
}

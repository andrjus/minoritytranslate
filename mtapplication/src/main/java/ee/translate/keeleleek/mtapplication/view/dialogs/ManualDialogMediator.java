package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.common.FileUtil;


public abstract class ManualDialogMediator extends Mediator {

	public final static String NAME = "{301E98DD-4EEC-4157-A49C-B63E364243D7}";
	
	public final static String PROGRAM_HTML_FILE = "/program.html";
	public final static String WIKITEXT_HTML_FILE = "/wikitext.html";
	
	protected static Logger LOGGER = LoggerFactory.getLogger(ManualDialogMediator.class);
	
	
	// INITIATION:
	public ManualDialogMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister()
	 {
//		try {
//			String str = FileUtil.read(Paths.get("/home/andf/data/shared/program-utf8.txt"));
//			String html = BotHolder.instance().parseText("en", str);
//			FileUtil.write(Paths.get("/home/andf/data/shared/program.html"), html);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
		try {
			String programHTML = FileUtil.readFromJar(PROGRAM_HTML_FILE);
			loadProgramPage(programHTML);
		} catch (Exception e) {
			LOGGER.warn("Failed to load program manual", e);
		}
		
		try {
			String wikitextHTML = FileUtil.readFromJar(WIKITEXT_HTML_FILE);
			loadWikitextPage(wikitextHTML);
		} catch (Exception e) {
			LOGGER.warn("Failed to load wikitext manual", e);
		}
		
	 }

	
	// IMPLEMENTATION:
	protected abstract void loadProgramPage(String html);
	protected abstract void loadWikitextPage(String html);

}

package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;

/**
 * Immutable language related preferences.
 */
public class LanguagesPreferences {

	private ArrayList<String> langCodes;
	private HashMap<String, Display> displays;
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public LanguagesPreferences()
	 {
		this.langCodes = new ArrayList<>();
		this.displays = new HashMap<>();
	 }
	
	public LanguagesPreferences(List<String> langCodes, Map<String, Display> displays)
	 {
		this.langCodes = new ArrayList<>(langCodes);
		this.displays = new HashMap<>(displays);
	 }

	public LanguagesPreferences(LanguagesPreferences preferences)
	 {
		this.langCodes = new ArrayList<>(preferences.langCodes);
		this.displays = new HashMap<>(preferences.displays);
	 }

	public static LanguagesPreferences create()
	 {
		LanguagesPreferences preferences = new LanguagesPreferences();
		return preferences;
	 }
	
	

	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	public List<String> getAllLangCodes() {
		return new ArrayList<>(this.langCodes);
	}
	
	public int getLangCodeCount() {
		return langCodes.size();
	}
	
	public boolean langCodesEqual(List<String> langCodes) {
		return this.langCodes.equals(langCodes);
	}
	
	public List<String> filterSrcLangCodes()
	 {
		ArrayList<String> result = new ArrayList<>();
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == Display.SOURCE) result.add(langCode);
		}
		return result;
	 }

	public List<String> filterDstLangCodes()
	 {
		ArrayList<String> result = new ArrayList<>();
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == Display.DESTINATION) result.add(langCode);
		}
		return result;
	 }

	public List<String> filterActiveLangCodes()
	 {
		ArrayList<String> result = new ArrayList<>();
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == Display.SOURCE || displays.get(langCode) == Display.DESTINATION) result.add(langCode);
		}
		return result;
	 }

	public String findLastLangCode(Display display)
	 {
		String result = null;
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == display) result = langCode;
		}
		return result;
	 }

	public String findLastSrcLangCode()
	 {
		return findLastLangCode(Display.SOURCE);
	 }

	public String findLastDstLangCode()
	 {
		return findLastLangCode(Display.DESTINATION);
	 }
	

	public LanguagesPreferences withLangCode(String langCode, Display display) {
		LanguagesPreferences preferences = new LanguagesPreferences(this);
		preferences.langCodes.remove(langCode);
		preferences.langCodes.add(langCode);
		preferences.displays.put(langCode, display);
		return preferences;
	}

	
	
	/* ******************
	 *                  *
	 *     Display      *
	 *                  *
	 ****************** */
	public Display getDisplay(String langCode) {
		Display display = displays.get(langCode);
		if (display == null) display = Display.NONE;
		return display;
	}
	
	
	
	/* ******************
	 *                  *
	 *     Utility      *
	 *                  *
	 ****************** */
	@Override
	public int hashCode()
	 {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displays == null) ? 0 : displays.hashCode());
		result = prime * result + ((langCodes == null) ? 0 : langCodes.hashCode());
		return result;
	 }

	@Override
	public boolean equals(Object obj)
	 {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		LanguagesPreferences other = (LanguagesPreferences) obj;
		if (displays == null) {
			if (other.displays != null) return false;
		} else if (!displays.equals(other.displays)) return false;
		if (langCodes == null) {
			if (other.langCodes != null) return false;
		} else if (!langCodes.equals(other.langCodes)) return false;
		return true;
	 }
	
}

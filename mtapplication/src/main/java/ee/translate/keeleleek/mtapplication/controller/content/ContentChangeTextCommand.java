package ee.translate.keeleleek.mtapplication.controller.content;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;

public class ContentChangeTextCommand extends SimpleCommand {

	private static TranslateWindowMediator windowMediator = null;
	
	@Override
	public void execute(INotification notification)
	 {
		Reference ref = (Reference) notification.getBody();
		String text = notification.getType();
		MinorityTranslateModel.content().changeText(ref, text);
		
		MinorityTranslateModel.content().changeSrcLangCode(ref, fetcWindowMediator().findSelectedSrcLangCode());
	 }
	
	private TranslateWindowMediator fetcWindowMediator() {
		if (windowMediator != null) return windowMediator;
		windowMediator = (TranslateWindowMediator) getFacade().retrieveMediator(TranslateWindowMediator.NAME);
		return windowMediator;
	}
	
}

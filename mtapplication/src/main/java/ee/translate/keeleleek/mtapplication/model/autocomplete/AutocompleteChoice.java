package ee.translate.keeleleek.mtapplication.model.autocomplete;

public class AutocompleteChoice {

	private String name;
	private String text;
	private String description;
	private int backOffset;
	private int frontOffset = 0;
	private int caretBackset;
	
	public AutocompleteChoice(String name, String text, String description, int backOffset, int caretBackset) {
		this.name = name;
		this.text = text;
		this.description = description;
		this.backOffset = backOffset;
		this.caretBackset = caretBackset;
	}
	
	public AutocompleteChoice(String misspellSuggestion, String description, int backOffset, int frontOffset) {
		this.name = misspellSuggestion;
		this.text = misspellSuggestion;
		this.description = description;
		this.backOffset = backOffset;
		this.frontOffset = frontOffset;
		this.caretBackset = 0;
	}
	
	
	public AutocompleteChoice(String name, String text) {
		this(name, text, "", 0, 0);
	}
	
	
	public String getName() {
		return name;
	}
	
	public String getText() {
		return text;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getTextBackset() {
		return backOffset;
	}
	
	public int getTextFronset() {
		return frontOffset;
	}
	
	public int getCaretBackset() {
		return caretBackset;
	}
	
	
}

package ee.translate.keeleleek.mtapplication.controller.suggestions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.suggestions.Suggestable;

public class SuggestLoadedLanguageCommand extends SimpleCommand {

	private final int RESULT_COUNT = 10;
	
	
	@Override
	public void execute(INotification notification)
	 {
		Suggestable suggestable = (Suggestable) notification.getBody();
		String searchTerm = suggestable.getSearchTerm();

		List<String> langCodes = MinorityTranslateModel.preferences().languages().getAllLangCodes();
		List<String> langNames = MinorityTranslateModel.wikis().getLangNames(langCodes);
		Collections.sort(langNames);
		
		String search = searchTerm.toLowerCase();
		ArrayList<String> results = new ArrayList<>();
		for (String langName : langNames) {
			if (langName.toLowerCase().startsWith(search)) results.add(langName);
			if (results.size() >= RESULT_COUNT) break;
		}
		
		String[] suggestions = results.toArray(new String[results.size()]);
		SuggestLanguageCommand.capitalise(suggestions);
		
		suggestable.suggest(suggestions);
	 }
	
}

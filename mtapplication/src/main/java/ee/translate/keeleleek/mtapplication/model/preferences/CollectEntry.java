package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;

public class CollectEntry implements TableEntry {
	
	public final static String REGEX_PREFIX = "REGEX:";
	final public static String COLLECT_WIKI_PUNCTUATION_REGEX = "[\\!\"#\\$%&'\\(\\)\\*\\+,\\-\\./:;\\<=\\>\\?@\\[\\\\\\]\\^_`\\{\\|\\}~]";
	
	private EntryFilter filter;
	private String find;
	
	
	// INIT
	public CollectEntry()
	 {
		this.filter = new EntryFilter();
		this.find = "";
	 }

	public CollectEntry(EntryFilter filter, String find)
	 {
		this.filter = filter;
		this.find = find;
	 }

	public CollectEntry(String srcLang, String dstLang, String find)
	 {
		this(new EntryFilter(srcLang, dstLang), find);
	 }

	public CollectEntry(EntryFilter filter, String find, String params)
	 {
		this(filter, find);
	 }
	
	public CollectEntry(CollectEntry other) {
		this.filter = new EntryFilter(other.filter);
		this.find = other.find;
	}
	
	
	// REGEX
	public RegexCollect createRegex()
	 {
		//TODO if (find.startsWith(REGEX_PREFIX)) return new RegexCollect(find.substring(REGEX_PREFIX.length()), params);
		String rgxFind = find;
		
		Pattern pParam = Pattern.compile(Pattern.quote("#{") + "(.*?)" + Pattern.quote("}"));
		Matcher mParam = pParam.matcher(rgxFind);
		
		ArrayList<String> findParams = new ArrayList<>();
		
		if (mParam.find()) {
			findParams.add(mParam.group(1));
		}

		rgxFind = "\\Q" + rgxFind; // treat as one (start)
		rgxFind = rgxFind + "\\E"; // treat as one (end)
		
		for (String param : findParams) {
			
			String userParam = "#{" + param + "}";

			if (rgxFind.endsWith(userParam + "\\E")) rgxFind = rgxFind + "$";
			if (rgxFind.startsWith("\\Q" + userParam)) rgxFind = "^" + rgxFind;
			
			rgxFind = rgxFind.replaceAll(Pattern.quote(userParam), Matcher.quoteReplacement("\\E(?<" + param + ">.*?)\\Q"));
			
		}
		
		rgxFind = rgxFind.replace("\\Q\\E", ""); // clean
		
		/*if (rgxFind.startsWith("_")) rgxFind = "(?<=\\s|\\p{Punct} )" + "\\Q" + rgxFind.substring(1, rgxFind.length());
		else if (rgxFind.startsWith("#")) rgxFind = "^" + "\\Q" + rgxFind;
		else rgxFind = "\\Q" + rgxFind; // treat as one (start)

		if (rgxFind.endsWith("_")) rgxFind = rgxFind.substring(0, rgxFind.length() - 1) + "\\E" + "(?=\\s|\\p{Punct} )";
		else if (rgxFind.endsWith("#")) rgxFind = rgxFind + "\\E" + "$";
		else rgxFind = rgxFind + "\\E"; // treat as one (end)
		
		rgxFind = rgxFind.replace("#", "\\E(.*?)\\Q"); // capture params
		
		rgxFind = rgxFind.replace("~", "\\E(?:.*?)\\Q"); // anything between
		
		rgxFind = rgxFind.replace("\\Q\\E", ""); // clean
*/		
		return new RegexCollect(rgxFind, findParams);
	 }

	
	// DATA
	public EntryFilter getFilter() {
		return filter;
	}

	public void setFilter(EntryFilter filter) {
		this.filter = filter;
	}

	public String getSrcLang() {
		return filter.getSrcLangRegex();
	}

	public String getDstLang() {
		return filter.getDstLangRegex();
	}

	public String getFind() {
		return find;
	}

	public void setFind(String find) {
		this.find = find;
	}

	
	// CONVERSION
	@Override
	public String toStringEntry(int c)
	 {
		switch (c) {
		case 0:
			String src = getFilter().getSrcLangRegex();
			String dst = getFilter().getDstLangRegex();
			return src + " ; " + dst;
			
		case 1:
			return getFind().replace("\n", "\\n");
			
		case 2:
			StringBuilder result = new StringBuilder();
			return result.toString();

		default:
			return null;
		}
	 }
	
	@Override
	public void fromStringEntry(int c, String value)
	 {
		switch (c) {
		case 0:
			String[] split = value.split(";", 2);
			String src = split[0].trim();
			String dst = split.length == 2 ? split[1].trim() : null;
			setFilter(new EntryFilter(src, dst));
			break;
			
		case 1:
			setFind(value.replace("\\n", "\n"));
			break;
			
		case 2:
			break;

		default:
			break;
		}
	 }
	

	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result + ((find == null) ? 0 : find.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectEntry other = (CollectEntry) obj;
		if (filter == null) {
			if (other.filter != null)
				return false;
		} else if (!filter.equals(other.filter))
			return false;
		if (find == null) {
			if (other.find != null)
				return false;
		} else if (!find.equals(other.find))
			return false;
		return true;
	}
	
	
	
}

package ee.translate.keeleleek.mtapplication.controller.login;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;

public class LoginRequestCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		getFacade().sendNotification(Notifications.LOGIN, notification.getBody(), notification.getType());
	 }
	
}

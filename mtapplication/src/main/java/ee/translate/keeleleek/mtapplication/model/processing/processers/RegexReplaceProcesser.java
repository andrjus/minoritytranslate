package ee.translate.keeleleek.mtapplication.model.processing.processers;

import java.util.HashMap;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;
import ee.translate.keeleleek.mtapplication.model.processing.RegexReplace;

public class RegexReplaceProcesser {

	final public static String SRC_TITLE_KEY = "${Src_title}";
	final public static String DST_TITLE_KEY = "${Dst_title}";
	final public static String SRC_TITLE_KEY_LOWER_CASE = "${src_title}";
	final public static String DST_TITLE_KEY_LOWER_CASE = "${dst_title}";
	
	
	public static String process(String srcTitle, String srcText, String dstTitle, String dstText, List<RegexCollect> regexCollects, List<RegexReplace> regexReplaces, HashMap<String, String> variableMap) throws PatternSyntaxException
	 {
		if (!dstTitle.isEmpty()) {
			variableMap.put(SRC_TITLE_KEY, srcTitle);
			variableMap.put(DST_TITLE_KEY, dstTitle);
			variableMap.put(SRC_TITLE_KEY_LOWER_CASE, decapitalise(srcTitle));
			variableMap.put(DST_TITLE_KEY_LOWER_CASE, decapitalise(dstTitle));
		}
		
		for (RegexCollect regexCollect : regexCollects) {
			regexCollect.collect(srcText, variableMap);
		}
		
		for (RegexReplace regexReplace : regexReplaces) {
			srcText = regexReplace.replace(srcText, variableMap);
		}
		
		return srcText;
	 }

	
	
	public static String capitalise(String str){
	    if(str.length() == 0) return str;
	    return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	public static String decapitalise(String str){
	    if(str.length() == 0) return str;
	    return str.substring(0, 1).toLowerCase() + str.substring(1);
	}
	
}

package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.LoginDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ExposableConfirmFX;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;


public class LoginDialogMediatorFX extends LoginDialogMediator implements ExposableConfirmFX {

	@FXML
	private Pane rootPane;
	@FXML
	private TextField usernameTextField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private CheckBox loginAutoCheckbox;

	@FXML
	private Button confirmButton;
	
	
	// INIT
	@FXML
	private void initialize() {
		usernameTextField.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if (confirmButton != null) confirmButton.setDisable(!isConfirmable());
			}
		});
		passwordField.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if (confirmButton != null) confirmButton.setDisable(!isConfirmable());
			}
		});
	}
	
	public void exposeConfirm(Button button)
	 {
		confirmButton = button;
		confirmButton.setDisable(!isConfirmable());
	 };
	
	private boolean isConfirmable() {
		return !usernameTextField.getText().isEmpty() && !passwordField.getText().isEmpty();
	}
	 
	
	// IMPLEMENT
	@Override
	protected String getUsername() {
		return usernameTextField.getText();
	}

	@Override
	protected String getPassword() {
		return passwordField.getText();
	}

	@Override
	protected boolean isAutoLogin() {
		return loginAutoCheckbox.isSelected();
	}
	
	@Override
	protected void setUsername(String text) {
		usernameTextField.setText(text);
	}

	@Override
	protected void setPassword(String text) {
		passwordField.setText(text);
	}

	
	// INHERIT (CONFIRM DECLINE DIALOG)
	@Override
	public void focus() {
		super.focus();
		usernameTextField.requestFocus();
	}
	
	
}

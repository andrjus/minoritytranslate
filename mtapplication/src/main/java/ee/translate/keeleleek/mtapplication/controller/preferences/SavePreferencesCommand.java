package ee.translate.keeleleek.mtapplication.controller.preferences;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class SavePreferencesCommand extends SimpleCommand {

	public final static Charset ENCODING = StandardCharsets.UTF_8;
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	
	@Override
	public void execute(INotification notification)
	 {
		Path path = LoadPreferencesCommand.FILE_PATH;
		
		PreferencesProxy preferences = MinorityTranslateModel.preferences();
		
		preferences.setFresh(false);
		
		Gson gson = new Gson();
		String json = gson.toJson(preferences);
		
		try {
			FileUtil.write(path, json);
		} catch (IOException e) {
			LOGGER.error("Failed to write preferences", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.preferences.save.failed"), Messages.getString("messages.preferences.save.failed.to").replaceFirst("#", path.toString()).replaceFirst("#",e.getMessage()));
		}
	 }

}

package ee.translate.keeleleek.mtapplication.model.plugins;

import ee.translate.keeleleek.mtpluginframework.PluginVersion;

public class PluginDescription {

	private PluginVersion pluginVersion = null;
	private PluginVersion spellerFrameworkVersion = null;
	private String spellerClass = null;
	private PluginVersion pullFrameworkVersion = null;
	private String pullClass = null;

	
	public PluginDescription(PluginVersion pluginVersion)
	{
		if (pluginVersion == null) throw new NullPointerException("Plugin version must not be null");
		
		this.pluginVersion = pluginVersion;
	}
	

	public PluginVersion getPluginVersion() {
		return pluginVersion;
	}
	
	
	public boolean isSpellerPlugin()
	{
		return spellerClass != null && spellerFrameworkVersion != null;
	}
	
	public PluginVersion getSpellerFrameworkVersion() {
		return spellerFrameworkVersion;
	}
	
	public String getSpellerClass() {
		return spellerClass;
	}
	
	public void setSpellerDescription(PluginVersion frameworkVersion, String spellerClass) {
		this.spellerFrameworkVersion = frameworkVersion;
		this.spellerClass = spellerClass;
	}
	
	
	public boolean isPullPlugin()
	{
		return pullClass != null && pullFrameworkVersion != null;
	}
	
	public PluginVersion getPullFrameworkVersion() {
		return pullFrameworkVersion;
	}
	
	public String getPullClass() {
		return pullClass;
	}
	
	public void setPullDescription(PluginVersion frameworkVersion, String pullClass) {
		this.pullFrameworkVersion = frameworkVersion;
		this.pullClass = pullClass;
	}
	
	
	@Override
	public String toString() {
		String pullVersion = pullFrameworkVersion != null ? pullFrameworkVersion.toString() : "null";
		String spellerVersion = spellerFrameworkVersion != null ? spellerFrameworkVersion.toString() : "null";
		return "{" + pluginVersion.toString() + ", " + spellerVersion + ": " + spellerClass + ", " + pullVersion + ": " + pullClass + "}";
	}
	
	
}

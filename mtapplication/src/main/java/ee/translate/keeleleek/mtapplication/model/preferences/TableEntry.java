package ee.translate.keeleleek.mtapplication.model.preferences;

public interface TableEntry {

	public String toStringEntry(int c);
	
	public void fromStringEntry(int c, String value);
	
}

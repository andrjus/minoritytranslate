package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtpluginframework.spellcheck.StripUtil;

public class TextAligner {

	public List<String> alignTextPartial(String text)
	 {
		if (!text.endsWith("\n")) text+= "\n";
		
		String template = StripUtil.stripAll(text);
		
		ArrayList<String> result = new ArrayList<>();
		int i = 0;
		int j = 0;
		int iLast = template.length() - 1;
		while (true) {
			
			i = template.indexOf('\n', i);
			if (i == -1 || i == iLast) {
				result.add(text.substring(j));
				break;
			}
			i++;
			
			result.add(text.substring(j, i));
			
			j = i;
		}
		
		
		return result;
	 }

	public List<String> alignText(String text)
	 {
		return alignTextPartial(text + "\n");
	 }
	
	public String unalignText(List<String> text)
	 {
		String result = "";
		
		for (String line : text) {
			result+= line;
		}
		
		if (result.endsWith("\n")) result = result.substring(0, result.length() - 1);
		
		return result;
	 }
	
	
	
}

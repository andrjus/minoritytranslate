package ee.translate.keeleleek.mtapplication.model.plugins;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugin;
import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugins;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;
import ee.translate.keeleleek.mtpluginframework.spellcheck.SpellerPlugin;

public class PluginsProxy extends Proxy {

	public final static String NAME = "{E2E3576C-956C-4D75-99EF-D05A4FF51B5E}";
	
	private static String PLUGINS_DIRECTORY = MinorityTranslate.ROOT_PATH + File.separator + "plugins";
	private static String PLUGIN_FILE = "plugin.properties";
	private static String PLUGIN_MAJOR_VERSION = "version_major";
	private static String PLUGIN_MINOR_VERSION = "version_minor";
	private static String PLUGIN_SPELLER_REQUIRED_MAJOR_VERSION = "speller_framework_required_major";
	private static String PLUGIN_SPELLER_REQUIRED_MINOR_VERSION = "speller_framework_required_minor";
	private static String PLUGIN_SPELLER_CLASS = "speller_class";
	private static String PLUGIN_PULL_REQUIRED_MAJOR_VERSION = "pull_framework_required_major";
	private static String PLUGIN_PULL_REQUIRED_MINOR_VERSION = "pull_framework_required_minor";
	private static String PLUGIN_PULL_CLASS = "pull_class";
	
	private static Logger LOGGER = LoggerFactory.getLogger(PluginsProxy.class);

	private ArrayList<MinorityTranslatePlugin> plugins = new ArrayList<>();
	
	private MinorityTranslatePlugins pluginsCallback = new MinorityTranslatePlugins() {

		@Override
		public SpellerPlugin findSpellerPlugin(String pluginName) {
			return MinorityTranslateModel.speller().getPlugin(pluginName);
		}
		
		@Override
		public PullPlugin findPullPlugin(String pluginName) {
			return MinorityTranslateModel.pull().getPlugin(pluginName);
		}
		
		@Override
		public String getStringMessage(String key) {
			return Messages.getString(key);
		}
		
		public Path getRootPath() {
			return Paths.get(MinorityTranslate.ROOT_PATH);
		};
		
		public Path getPluginsPath() {
			return Paths.get(PLUGINS_DIRECTORY);
		};
		
	};
	
	
	// INIT
	public PluginsProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		// filter jars
		File pluginsDir = new File(PLUGINS_DIRECTORY);
		if (!pluginsDir.isDirectory()) pluginsDir.mkdirs();
		File[] pluginsFiles = pluginsDir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".jar");
			}
		});

		// premade plugins
		MinorityTranslateModel.pull().addPlugin(new CopyReplacePastePlugin(), PullProxy.PULL_FRAMEWORK_VERSION);
		MinorityTranslateModel.speller().addPlugin(new DisabledSpellerPlugin(), SpellerProxy.SPELLER_FRAMEWORK_VERSION);
		
		// load plugins
		for (File file : pluginsFiles) {
			
			LOGGER.info("Loading " + file);
			
			JarFile jarFile = null;
			try {
				jarFile = new JarFile(file);
			} catch (Exception e) {
				LOGGER.info("Failed to load jar file " + file);
				continue;
			}
			
			// read description
			PluginDescription description = readPluginDescription(jarFile);
            if (description == null) {
            	LOGGER.error("File " + file.getAbsolutePath() + " is missing plugin description!");
            	continue;
            }

            // load
            boolean invalid = true;
            
            //  speller
            if (description.isSpellerPlugin()) {
            	
            	SpellerPlugin plugin = MinorityTranslateModel.speller().loadPlugin(file, description);
            	
            	if (plugin != null) {
            		try {
						plugin.setup(pluginsCallback);
					} catch (Throwable e) {
						LOGGER.error("Plugin setup failed", e);
					}
            		invalid = false;
            	}
            	
            }

            //  pull
            if (description.isPullPlugin()) {
            	
            	PullPlugin plugin = MinorityTranslateModel.pull().loadPlugin(file, description);
            	
            	if (plugin != null) {
            		try {
						plugin.setup(pluginsCallback);
					} catch (Throwable e) {
						LOGGER.error("Plugin setup failed", e);
					}
            		invalid = false;
            	}
            	
            }
			
            //  invalid
            if (invalid) {
            	LOGGER.error("File " + file.getAbsolutePath() + " is not a valid plugin!");
            }
            
		}
	 }
	
	@Override
	public void onRemove()
	 {
		
	 }
	
	public void close()
	 {
		// close all plugins
		for (MinorityTranslatePlugin plugin : plugins) {
			try {
				LOGGER.info("Closing " + plugin.getName() + " plugin");
				plugin.close();
			} catch (Exception e) {
				LOGGER.error("Failed to close " + plugin.getName() + " plugin", e);
			}
		}
	 }
	
	
	private PluginDescription readPluginDescription(JarFile jarFile)
	{
		ZipEntry zipEntry = jarFile.getEntry(PLUGIN_FILE);
		if (zipEntry == null) return null;
		
		Properties properties = new Properties();
		
		try {
			
			InputStream in = null;
			try {
				in = jarFile.getInputStream(zipEntry);
				properties.load(in);
			} finally {
				if (in != null) in.close();
			}
			
		} catch (IOException e) {
			LOGGER.error("Failed to read plugin description", e);
			return null;
		}

		PluginDescription pluginDescription = null;
		
		String majorStr = properties.getProperty(PLUGIN_MAJOR_VERSION);
		String minorStr = properties.getProperty(PLUGIN_MINOR_VERSION);
		String spellerReqMajorStr = properties.getProperty(PLUGIN_SPELLER_REQUIRED_MAJOR_VERSION);
		String spellerReqMinorStr = properties.getProperty(PLUGIN_SPELLER_REQUIRED_MINOR_VERSION);
		String spellerClass = properties.getProperty(PLUGIN_SPELLER_CLASS);
		String pullReqMajorStr = properties.getProperty(PLUGIN_PULL_REQUIRED_MAJOR_VERSION);
		String pullReqMinorStr = properties.getProperty(PLUGIN_PULL_REQUIRED_MINOR_VERSION);
		String pullClass = properties.getProperty(PLUGIN_PULL_CLASS);
		
		try {

			if (majorStr != null && minorStr != null) {
				
				PluginVersion pluginVersion = new PluginVersion(Integer.parseInt(majorStr), Integer.parseInt(minorStr));
				pluginDescription = new PluginDescription(pluginVersion);
				
			} else {
				LOGGER.error("Missing plugin version");
				return null;
			}

			if (spellerReqMajorStr != null && spellerReqMinorStr != null && spellerClass != null) {
				PluginVersion spellerReqVersion = new PluginVersion(Integer.parseInt(spellerReqMajorStr), Integer.parseInt(spellerReqMinorStr));
				pluginDescription.setSpellerDescription(spellerReqVersion, spellerClass);
			}

			if (pullReqMajorStr != null && pullReqMinorStr != null && pullClass != null) {
				PluginVersion pullReqVersion = new PluginVersion(Integer.parseInt(pullReqMajorStr), Integer.parseInt(pullReqMinorStr));
				pluginDescription.setPullDescription(pullReqVersion, pullClass);
			}
			
		} catch (NumberFormatException e) {
			LOGGER.error("Failed to parse plugin description", e);
		}
		
		return pluginDescription;
	}
	
		
	
	
}

package ee.translate.keeleleek.mtapplication.model.lists;

import java.util.List;

import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.content.WikiRequest;

public class ListSuggestion {

	private WikiRequest req;
	private List<String> type;
	
	public ListSuggestion(WikiRequest req, List<String> type) {
		super();
		this.req = req;
		this.type = type;
	}

	public WikiRequest getRequest() {
		return req;
	}

	public WikiReference getRef() {
		return req.getOriginal();
	}

	public String getQID() {
		return req.getQid();
	}
	
	public String getLangCode() {
		return req.getOriginal().getLangCode();
	}
	
	public String getTitle() {
		return req.getOriginal().getNamespacedWikiTitle();
	}
	
	public List<String> getTypeList() {
		return type;
	}

	@Override
	public int hashCode() {
		return req.hashCode();
	}

	public boolean isTranslated(String langCode) {
		return req.getQidMeta().getLanglinks().containsKey(langCode);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ListSuggestion) {
			ListSuggestion cobj = (ListSuggestion) obj;
			return getQID().equals(cobj.getQID());
		} else return false;
	}
	
	@Override
	public String toString() {
		return req.toString();
	}
	
	
	
}

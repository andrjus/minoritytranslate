package ee.translate.keeleleek.mtapplication.controller.populating;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class AddLinksCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		final String articleName = (String) notification.getBody();
		final String langCode = notification.getType();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				String text = MinorityTranslateModel.bots().retrieveText(langCode, articleName);
				
				ArrayList<String> titles = new ArrayList<>();
				ArrayList<String> langCodes = new ArrayList<>();
				collectLinks(text, langCode, titles, langCodes);

				for (int i = 0; i < titles.size(); i++) {
					
					String title = titles.get(i);
					String langCode = langCodes.get(i);
					
					MinorityTranslateModel.queuer().requestArticle(langCode, title);
					
				}

				LoggerFactory.getLogger(getClass()).info("Adding " + titles.size() + " links");
				
			}
		}).start();
		
	 }
	
	
	public static void collectLinks(String text, String textLangCode, ArrayList<String> titlesOut, ArrayList<String> langCodesOut) {
		
		Pattern p = Pattern.compile("\\[\\[(.*?)\\]\\]", Pattern.DOTALL | Pattern.MULTILINE);
		Matcher m = p.matcher(text);

		Pattern p2 = Pattern.compile(":(.*?):", Pattern.DOTALL | Pattern.MULTILINE);
		
		while (m.find()) {
			String title = m.group(1);
			String langCode = textLangCode;

			System.out.println(title);
			
			// Handle language:
			if (title.contains(":")) {
				Matcher m2 = p2.matcher(title);
				if (!m2.find()) continue;
				langCode = m2.group(1);
				title = title.replace(":" + langCode + ":", "");
			}

			// Handle title:
			int i = title.indexOf('|');
			if (i != -1) title = title.substring(0, i);
			
			titlesOut.add(title);
			langCodesOut.add(langCode);
		}
		
	}
	
}
